#include <string.h>
#include "blinker.hpp"
#include "form.hpp"

Blinker::Blinker(){
    orientation = "vertical";
}

Blinker::~Blinker(){
}

std::string Blinker::getOrientation(){
   return orientation;
}
void Blinker::setOrientation(std::string orientation){
   this->orientation = orientation;
}


void Blinker::place_form(bool matrix[26][78]){
    if (orientation == "vertical"){
        matrix[x_position][y_position] = true;
        matrix[x_position+1][y_position] = true;
        matrix[x_position+2][y_position] = true;
    } else {
        matrix[x_position][y_position] = true;
        matrix[x_position][y_position+1] = true;
        matrix[x_position][y_position+2] = true;
    }

}

void Blinker::blinker_simulate(bool matrix[26][78], bool temp_matrix[26][78]){
    place_form(matrix);
    place_form(temp_matrix);
}
