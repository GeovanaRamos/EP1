#include <iostream>
#include "gosperglidergun.hpp"
//#include "form.hpp"
#include "block.hpp"
#include "glider.hpp"
#include "blinker.hpp"
#include "double.hpp"
#include "single.hpp"

GosperGliderGun::GosperGliderGun(){
}

GosperGliderGun::~GosperGliderGun(){
}


void GosperGliderGun::initial_pattern(bool cells[26][78], bool new_generation[26][78]){
// Criando matriz gosperglidergun com formas já existentes
// gosperglidergun é composta por 2 blocks, 4 blinkers, 2 doubles, 7 singles e 1 glider(opcional)

    Block block;
    Glider glider;
    Blinker blinker;
    Double double_cell;
    Single single_cell;

    block.setX_Position(5);
    block.setY_Position(1);
    block.place_form(cells);
    block.place_form(new_generation);
    block.setX_Position(3);
    block.setY_Position(35);
    block.place_form(cells);
    block.place_form(new_generation);

    glider.setX_Position(11);
    glider.setY_Position(25);
    glider.place_form(cells);
    glider.place_form(new_generation);

    blinker.setX_Position(5);
    blinker.setY_Position(11);
    blinker.setOrientation("vertical");
    blinker.place_form(cells);
    blinker.place_form(new_generation);
    blinker.setX_Position(5);
    blinker.setY_Position(17);
    blinker.setOrientation("vertical");
    blinker.place_form(cells);
    blinker.place_form(new_generation);
    blinker.setX_Position(3);
    blinker.setY_Position(21);
    blinker.setOrientation("vertical");
    blinker.place_form(cells);
    blinker.place_form(new_generation);
    blinker.setX_Position(3);
    blinker.setY_Position(22);
    blinker.setOrientation("vertical");
    blinker.place_form(cells);
    blinker.place_form(new_generation);

    double_cell.setX_Position(9);
    double_cell.setY_Position(13);
    double_cell.setOrientation("horizontal");
    double_cell.place_form(cells);
    double_cell.place_form(new_generation);
    double_cell.setX_Position(3);
    double_cell.setY_Position(13);
    double_cell.setOrientation("horizontal");
    double_cell.place_form(cells);
    double_cell.place_form(new_generation);
    double_cell.setX_Position(1);
    double_cell.setY_Position(25);
    double_cell.setOrientation("vertical");
    double_cell.place_form(cells);
    double_cell.place_form(new_generation);
    double_cell.setX_Position(6);
    double_cell.setY_Position(25);
    double_cell.setOrientation("vertical");
    double_cell.place_form(cells);
    double_cell.place_form(new_generation);

    single_cell.setX_Position(4);
    single_cell.setY_Position(12);
    single_cell.place_form(cells);
    single_cell.place_form(new_generation);
    single_cell.setX_Position(8);
    single_cell.setY_Position(12);
    single_cell.place_form(cells);
    single_cell.place_form(new_generation);
    single_cell.setX_Position(6);
    single_cell.setY_Position(15);
    single_cell.place_form(cells);
    single_cell.place_form(new_generation);
    single_cell.setX_Position(4);
    single_cell.setY_Position(16);
    single_cell.place_form(cells);
    single_cell.place_form(new_generation);
    single_cell.setX_Position(8);
    single_cell.setY_Position(16);
    single_cell.place_form(cells);
    single_cell.place_form(new_generation);
    single_cell.setX_Position(6);
    single_cell.setY_Position(18);
    single_cell.place_form(cells);
    single_cell.place_form(new_generation);
    single_cell.setX_Position(6);
    single_cell.setY_Position(23);
    single_cell.place_form(cells);
    single_cell.place_form(new_generation);
    single_cell.setX_Position(2);
    single_cell.setY_Position(23);
    single_cell.place_form(cells);
    single_cell.place_form(new_generation);

}
