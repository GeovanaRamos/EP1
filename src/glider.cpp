#include "glider.hpp"
#include "form.hpp"

Glider::Glider(){
}

Glider::~Glider(){
}


void Glider::place_form(bool matrix[26][78]){
    matrix[x_position][y_position] = true;
    matrix[x_position+1][y_position] = true;
    matrix[x_position-1][y_position-1] = true;
    matrix[x_position][y_position+1] = true;
    matrix[x_position+1][y_position-1] = true;
}

void Glider::glider_simulate(bool matrix[26][78], bool temp_matrix[26][78]){
    place_form(matrix);
    place_form(temp_matrix);
}
