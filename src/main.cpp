#include <iostream>
#include <cstdlib>
#include<unistd.h>
#include "game.hpp"

using namespace std;

int main(int argc, char ** argv) {

    int i, j, count_gen, generations ,number_of_neighbors;


    while(1){
        Game game_of_life;
    //• Menu Inicial
        generations = game_of_life.menu();
        if (generations == -1){
            return 0;
        }
        game_of_life.show();

        for(count_gen=0; count_gen<generations; count_gen++){
            for(i=0;i<26;i++){
                for(j=0;j<78;j++){
                    number_of_neighbors = game_of_life.count_neighbors(i,j);
                    game_of_life.live_or_die(number_of_neighbors,i,j);
                }
            }
            game_of_life.copy_new_gen_to_cells();
            game_of_life.show();
            usleep(110000);
            system("clear");
        }
    }

   return 0;
}
