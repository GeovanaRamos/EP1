#include <iostream>
#include <cstdlib>
#include "game.hpp"
#include "gosperglidergun.hpp"
#include "block.hpp"
#include "glider.hpp"
#include "blinker.hpp"

using namespace std;

Game::Game(){
   for (int i=0;i<26;i++){
       for(int j=0;j<78;j++){
           cells[i][j] = false; //Falso, célula está morta - matriz principal
           new_generation[i][j] = false; // Falso, célula está morta - matriz temporária
       }
   }
}

Game::~Game(){
}

bool Game::getCells(){
   return cells[26][78];
}
void Game::setCells(bool cells){
   this->cells[26][78] = cells;
}

bool Game::getNew_Generation(){
   return new_generation[26][78];
}
void Game::setNew_Generation(bool new_generation){
   this->new_generation[26][78] = new_generation;
}

int Game::menu(){
    int answer;

    system("clear");
    cout << "\n\t\t    ================ MENU ================\t\t\t" << '\n';
    cout << "\n\t\t\t   Bem vindo ao Game of Life" << "\n\n";
    cout << "O que gostaria de simular?" << "\n";
    cout << "Blinker(1)" << '\n';
    cout << "Block(2)" << '\n';
    cout << "Glider(3)" << '\n';
    cout << "Gosper Glider Gun(4)" << '\n';
    cout << "Sair(5)" << '\n';
    cin >> answer;
    while (answer < 1 || answer > 5 ){
        cout << "Opção inválida. Digite um número entre 1 e 5:"<< '\n';
        cin >> answer;
    }

    switch(answer){
		case 1:{
			system("clear");
            Blinker blinker_sample;
            cout << "Digite a coordenada X (entre 0 e 21):"<< '\n';
            cin >> answer;
            while (answer < 0 || answer > 21){
                cout << "Coordenada X inválida. Digite um número entre 0 e 21:"<< '\n';
                cin >> answer;
            }
            blinker_sample.setX_Position(answer+1);
            cout << "Digite a coordenada Y (entre 0 e 73):" << '\n';
            cin >> answer;
            while(answer < 0 || answer > 73){
                cout << "Coordenada Y inválida. Digite um número entre 0 e 73:"<< '\n';
                cin >> answer;
            }
            blinker_sample.setY_Position(answer+1);
            blinker_sample.blinker_simulate(cells, new_generation);
            break;
        }
        case 2:{
			system("clear");
            Block block_sample;
            cout << "Digite a coordenada X (entre 0 e 21):"<< '\n';
            cin >> answer;
            while (answer < 0 || answer > 21){
                cout << "Coordenada X inválida. Digite um número entre 0 e 21:"<< '\n';
                cin >> answer;
            }
            block_sample.setX_Position(answer+1);
            cout << "Digite a coordenada Y (entre 0 e 73):" << '\n';
            cin >> answer;
            while(answer < 0 || answer > 73){
                cout << "Coordenada Y inválida. Digite um número entre 0 e 73:"<< '\n';
                cin >> answer;
            }
            block_sample.setY_Position(answer+1);
            block_sample.block_simulate(cells, new_generation);
            break;
		}
        case 3:{
            system("clear");
            Glider glider_sample;
            cout << "Digite a coordenada X (entre 0 e 21):"<< '\n';
            cin >> answer;
            while (answer < 0 || answer > 21){
                cout << "Coordenada X inválida. Digite um número entre 0 e 21:"<< '\n';
                cin >> answer;
            }
            glider_sample.setX_Position(answer+1);
            cout << "Digite a coordenada Y (entre 0 e 73):" << '\n';
            cin >> answer;
            while(answer < 0 || answer > 73){
                cout << "Coordenada Y inválida. Digite um número entre 0 e 73:"<< '\n';
                cin >> answer;
            }
            glider_sample.setY_Position(answer+1);
            glider_sample.glider_simulate(cells, new_generation);
            break;
        }
        case 4:{
            system("clear");
            GosperGliderGun gosper_glider_gun;
            gosper_glider_gun.initial_pattern(cells, new_generation);
            break;
        }
        case 5:{
            return -1;
		}
        default:{
			cout << "Opção inválida" << endl;
	    }
    }

    cout << "Quantas gerações deseja simular?" << '\n';
    cin >> answer;
    while(answer < 0){
        cout << "Número de gerações não pode ser negativo. Digite novamente:"<< '\n';
        cin >> answer;
    }
    system("clear");

    return answer;

}

int Game::count_neighbors(int i, int j){
    int count=0;
    int p,q;

    for (p = i-1; p <= i+1; p++){
        for(q= j-1; q<= j+1; q++){
            if (p == i && q == j){
                continue;// Evitando a própria célula
            } else if (p<0 || q<0){
                continue;// Evitando index negativo
            } else if (p>26 || q>78){
                continue;// Evitando index acima da borda
            } else if (cells[p][q]){
                count++;
            } else{
                continue;
            }
        }
    }

    return count;
}

void Game::live_or_die(int count, int i, int j){

    if (cells[i][j] && count<2){//Qualquer célula viva com menos de dois vizinhos vivos morre de solidão.
        new_generation[i][j] = false;
    } else if (cells[i][j] && count>3){//Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação.
        new_generation[i][j] = false;
    } else if (!(cells[i][j]) && count==3 ){//Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva.
        new_generation[i][j] = true;
    } else if (cells[i][j] && (count==2 || count ==3) ){//Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração.
        new_generation[i][j] = true;
    }
}


void Game::copy_new_gen_to_cells(){
    for (int i=0;i<26;i++){
        for(int j=0;j<78;j++){
            cells[i][j] = new_generation[i][j];
        }
    }
}

void Game::show(){

    for (int i=1;i<24;i++){// ilusão de borda infinita
        for(int j=1;j<76;j++){// ilusão de borda infinita
            if(cells[i][j]){
                cout << 0;
            }else{
                cout << "-";
            }
        }
        cout << endl;
    }

}
