#ifndef BLINKER_HPP
#define BLINKER_HPP
#include <string>
#include "form.hpp"

class Blinker: public Form {
    protected:
        std::string orientation;

    public:
        Blinker();
        ~Blinker();

        std::string getOrientation();
    	void setOrientation(std::string orientation);

        void place_form(bool matrix[26][78]);
        void blinker_simulate(bool matrix[26][78], bool temp_matrix[26][78]);// simulates a lonely blinker

};

#endif
