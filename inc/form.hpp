#ifndef FORM_HPP
#define FORM_HPP

class Form { // base class

    protected:
        int x_position;
        int y_position;

    public:
        Form();
        ~Form();

        int getX_Position();
    	void setX_Position(int x_position);
        int getY_Position();
    	void setY_Position(int y_position);


        virtual void place_form(bool matrix[26][78])=0; //places forms in the main matrix


};

#endif
