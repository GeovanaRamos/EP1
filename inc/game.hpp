#ifndef GAME_HPP
#define GAME_HPP

class Game {
    protected:
       bool cells[26][78];//main matrix
       bool new_generation[26][78];// temporary matrix

    public:
       Game();
       ~Game();

       bool getCells();
       void setCells(bool cells);
       bool getNew_Generation();
       void setNew_Generation(bool new_generation);

       int menu();
       int count_neighbors(int i, int j);// counts the number of neighbors around the cell
       void live_or_die(int count, int i, int j); // aplies the four rules and defines the cell state in the temp_matrix
       void copy_new_gen_to_cells();// copy the cells and their states from the temp_matrix to the main matrix
       void show(); // prints the main matrix


};

#endif
