#ifndef SINGLE_HPP
#define SINGLE_HPP
#include "form.hpp"

class Single: public Form { // set of only one cell

    public:
        Single();
        ~Single();

        void place_form(bool matrix[26][78]);

};

#endif
