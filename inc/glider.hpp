#ifndef GLIDER_HPP
#define GLIDER_HPP
#include "form.hpp"

class Glider: public Form {

    public:
        Glider();
        ~Glider();

        void place_form(bool matrix[26][78]);
        void glider_simulate(bool matrix[26][78], bool temp_matrix[26][78]);// simulates a lonely glider

};

#endif
