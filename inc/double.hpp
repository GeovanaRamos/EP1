#ifndef DOUBLE_HPP
#define DOUBLE_HPP
#include <string>
#include "form.hpp"


class Double: public Form { // set of two cells
    protected:
        std::string orientation;

    public:
        Double();
        ~Double();

        std::string getOrientation();
    	void setOrientation(std::string orientation);

        void place_form(bool matrix[26][78]);


};

#endif
