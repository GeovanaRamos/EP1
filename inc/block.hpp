#ifndef BLOCK_HPP
#define BLOCK_HPP
#include "form.hpp"

class Block: public Form {

    public:
        Block();
        ~Block();

        void place_form(bool matrix[26][78]);
        void block_simulate(bool matrix[26][78], bool temp_matrix[26][78]);// para simulação de um só block


};

#endif
