#ifndef GOSPERGLIDERGUN_HPP
#define GOSPERGLIDERGUN_HPP


class GosperGliderGun {
    public:

        GosperGliderGun();
        ~GosperGliderGun();
        
        void initial_pattern(bool cells[26][78], bool new_generation[26][78]);
};

#endif
