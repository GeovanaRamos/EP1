# Game of Life

### Compilar
  - Make clean -> para limpar as pastas bin e obj (pode evitar erros!)
  - Make -> para compilar
  - Make run -> para iniciar o jogo

### Menu
O usuário terá 5 opções de menu:

  - Blinker
  - Block
  - Glider
  - Gosper Glider Gun
  - Sair

 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para selecionar uma das opções, basta digitar o número associado ao nome da simulação que deseja iniciar e apertar enter. Se desejar encerrar o jogo, aperte "5". 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para a Gosper Glider Gun o usuário deverá digitar um número de gerações positivo. Para as 3 primeiras simulações, além do número de gerações, o usuário pode escolher a posição X e Y do objeto, de acordo com o intervalo de valores dado pelo programa.


### Blinker

   >   ![N|Solid](https://upload.wikimedia.org/wikipedia/commons/9/95/Game_of_life_blinker.gif)

### Block

   >   ![N|Solid](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Game_of_life_block_with_border.svg/66px-Game_of_life_block_with_border.svg.png)


### Glider

   >   ![N|Solid](https://upload.wikimedia.org/wikipedia/commons/f/f2/Game_of_life_animated_glider.gif)


### Gosper Glider Gun

   >   ![N|Solid](https://gitlab.com/oofga/eps_2017_2/ep1/wikis/glider-gun.gif)
